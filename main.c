#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

bool valIn(int val, int *arr, int size){
    int i;
    for (i=0; i < size; i++) {
        if (arr[i] == val)
            return true;
    }
    return false;
}

int main()
{
    bool debug = false;
    int c;
    char top3[3][129];
    char cur[129];
    for (int i = 0; i < 3; i++)
    {
        top3[i][0] = '\0';
    }
    cur[128] = '\0';

    int i = 0;

    while ((c = getchar()) != EOF)
    {
        if (isgraph(c))
        {
            if (i <= 127)
            {
                cur[i] = c;
            }
            i++;
        }
        else
        {
            //word finished
            cur[i] = '\0';
            if (debug) printf("word = '%s'\n", cur);
            if (debug) printf("top3: '%s' '%s' '%s'\n", top3[0], top3[1], top3[2]);

            //compute shortest top3
            unsigned long low = strlen(top3[0]);
            int ldex = 0;
            for (int j = 1; j < 3; j++)
            {
                unsigned long nl = strlen(top3[j]);
                if (nl < low)
                {
                    low = nl;
                    ldex = j;
                }
            }

            if (debug) printf("ldex = %i (%d)\n", ldex, (int)low);

            //only update if new word is bigger than shortest top3
            if (strlen(cur) > low)
            {
                //& is not the same as any top3
                int cv0 = strcmp(cur, top3[0]);
                int cv1 = strcmp(cur, top3[1]);
                int cv2 = strcmp(cur, top3[2]);
                if (cv0 != 0 && cv1 != 0 && cv2 != 0)
                {
                    strcpy(top3[ldex], cur);
                }
            }
            i = 0;
        }
    }

    int printed[3] = {-1,-1,-1};
    int printedNum = 0;
    while (printedNum < 3)
    {
        int hi = 0;
        int hidex = -1;
        for (int i = 0; i < 3; i++)
        {
            bool notIn = !valIn(i, printed, 3);
            unsigned long l = strlen(top3[i]);
            bool nh = strlen(top3[i]) > hi;
            if (notIn && nh)
            {
                hi = l;
                hidex = i;
            }
        }
        printf("%s\n", top3[hidex]);
        printed[printedNum] = hidex;
        printedNum++;
    }
}


